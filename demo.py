from pyspark.sql import SparkSession

spark = SparkSession \
    .builder \
    .enableHiveSupport() \
    .master("spark://192.168.71.110:7077") \
    .appName("test") \
    .getOrCreate()

if __name__ == '__main__':
    spark.sql("show databases").show()
    spark.sql("show tables").show()
    # data = spark.sql("CREATE TABLE IF NOT EXISTS src (key INT, value STRING)")
    # spark.sql("select * from test").show()
